import { AngularnewPage } from './app.po';

describe('angularnew App', () => {
  let page: AngularnewPage;

  beforeEach(() => {
    page = new AngularnewPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
