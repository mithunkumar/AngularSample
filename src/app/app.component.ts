import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
template: `<div style="padding:5px">
<ul class="nav nav-tabs">
<li><a  routerLink="home" >Home</a></li>
<li><a routerLink="employeelist">Employees</a></li>
</ul>
<router-outlet></router-outlet>
</div>
`,
    styleUrls: ['./app.component.css']
})

export class AppComponent {

  
}
