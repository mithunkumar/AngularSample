import { Component, OnInit } from '@angular/core';
//importing the interface
import { IEmployes } from '../employees';
//importing the service
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-employeelist',
  templateUrl: './employeelist.component.html',
  styleUrls: ['./employeelist.component.css'],
  providers:[ ServiceService]
})
export class EmployeelistComponent implements OnInit {
employees: IEmployes[];
selectedEmployeeCountRadioButton: string = 'All';



constructor(private _serviceService: ServiceService)
 {
  this.employees = this._serviceService.getEmployees();
}
ngOnInit(){
  this.employees = this._serviceService.getEmployees();
}

getTotalEmployeesCount(): number {
  return this.employees.length;
}
getMaleTotalEmployeesCount() :number 
{
return this.employees.filter(e => e.gender == 'male').length;
}
getFemaleTotalEmployeesCount() :number 
{
return this.employees.filter(e => e.gender == 'female').length;
}

}
