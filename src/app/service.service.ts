//for dependency injection
import { Injectable } from '@angular/core';

import {Observable } from 'rxjs/Observable';
import {Http, Response} from '@angular/http';

//interface
import { IEmployes } from './employees';

@Injectable()
export class ServiceService {

  getEmployees() : IEmployes[]  {
      return [

  {code: 'empl01', firstname:'mithun', lastname:'kumar',gender:'male',location:'mpl'},
  {code: 'empl02', firstname:'bruce', lastname:'slanger',gender:'male',location:'pvn'},
  {code: 'empl03', firstname:'sha', lastname:'shabu',gender:'female',location:'uk'},
  {code: 'empl04', firstname:'lucky', lastname:'mar',gender:'male',location:'swdn'},

];
}
}
