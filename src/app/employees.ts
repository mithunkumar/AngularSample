export interface IEmployes
{
    code: string;
    firstname: string;
    lastname: string;
    gender: string;
    location: string;
}
