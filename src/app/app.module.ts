import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule,Routes } from '@angular/router';
import { SampleComponent } from './sample/sample.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { HomeComponent } from './home/home.component';
import { NotfoundComponent } from './notfound/notfound.component';
import {FormsModule} from '@angular/forms';
import{HttpModule} from '@angular/http';



const appRoutes: Routes = [
  {path : 'home', component: HomeComponent},
  {path: 'employeelist', component: EmployeelistComponent },
  //by default to open the home page
  {path: '', redirectTo:'/home', pathMatch: 'full'},
  //wildcard(**) when the requested page is not found  it will redirect to notfound page.
  {path:'**', component:NotfoundComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    SampleComponent,
    EmployeesComponent,
    EmployeelistComponent,
    HomeComponent,
    NotfoundComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpModule,RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
